# Changelog
## 1.0.0

The first version of the package including :
 - Service to call the Open-Meteo Geocoding API

The package doesn't include the 'protobuf' API format.