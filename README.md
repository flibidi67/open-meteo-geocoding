# 🌤 Open-Meteo Geocoding API

[![Latest Version on Packagist](https://img.shields.io/packagist/v/flibidi67/open-meteo-geocoding.svg?style=flat-square)](https://packagist.org/packages/flibidi67/open-meteo-geocoding)
[![Total Downloads](https://img.shields.io/packagist/dt/flibidi67/open-meteo-geocoding.svg?style=flat-square)](https://packagist.org/packages/flibidi67/open-meteo-geocoding)

Open-Meteo collaborates with National Weather Services providing Open Data with 11 to 2 km resolution. Our high performance APIs select the best weather model for your location and provide data as a simple JSON API.

APIs are free without any API key for open-source developers and non-commercial use. You can embed them directly into your app.

Official documentation is available [here](https://open-meteo.com/en/docs/geocoding-api).

## Installation
You can install the package via composer:
```bash
composer require flibidi67/open-meteo-geocoding
```

## Usage
### Native PHP
```php
require_once('/path/to/vendor/flibidi67/open-meteo-geocoding/src/Service/GeocodingService');
use Flibidi67\OpenMeteoGeocoding\Service\GeocodingService;

$service = new GeocodingService();
```

### Symfony
If you want to use DependencyInjection, you have to add this to your `config/services.yaml`:
```yaml
services:
    # your other configuration
  
    Flibidi67\OpenMeteoGeocoding\Service\GeocodingService:
        autowire: true
```

Then you can use it like that :
```php
use Flibidi67\OpenMeteoGeocoding\Service\GeocodingService;
public function myFunction(GeocodingService $geocodingService) {
    $geocodingService->get('Stras');
}

// or
public function myFunction2() {
    $geocodingService = new GeocodingService();
    $geocodingService->get('Stras');
}
```

## Available service's methods
Here are all the available service's methods :
 - `setLanguage`, to set the language, accepted values are `en`, `de`, `fr`, `es`, `it`, `pt`, `ru`, `tr` and `hi`, by default `en`.
 - `setFormat`, to set the format, to be honest, useless because only `json` is currently supported
 - `setCount`, to set how much results do you want to retrieve (min 1, max 100), by default `10`.
 - `setName`, to set the search field, must be at least 3 characters.
 - `setCurlCheckSSLCertificate`, to disable curl SSL verification (not recommended, but just in case), by default `true`
 - `get`, retrieve the results, you can give the search field when calling this method, see example above

If you are manually instanciate the service, you can give an array of settings to avoid you using the set methods, like this :
```php
    $geocodingService = new GeocodingService(
        'language' => 'fr',
        'name' => 'Stra',
        'format' => 'json',
        'count' => 42,
        'curlCheckSSLCertificate' => false
    );
    // or just some of them
    $geocodingService = new GeocodingService(
        'language' => 'fr',
        'count' => 42
    );
```