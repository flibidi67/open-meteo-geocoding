<?php

namespace Flibidi67\OpenMeteoGeocoding\Service;

use Exception;

class GeocodingService {
    const API_URL = "https://geocoding-api.open-meteo.com/v1/search";
    const FORMAT_JSON = "json";
    const AVAILABLE_FORMAT = [
        self::FORMAT_JSON
    ];
    const DEFAULT_FORMAT = self::FORMAT_JSON;

    const AVAILABLE_LANGUAGES = [
        "en",
        "de",
        "fr",
        "es",
        "it",
        "pt",
        "ru",
        "tr",
        "hi"
    ];
    const DEFAULT_LANGUAGE = "en";

    const MIN_COUNT = 1;
    const MAX_COUNT = 100;
    const DEFAULT_COUNT = 10;

    const MIN_NAME_LENGTH = 3;

    private ?string $language = null;
    private ?string $name = null;
    private ?string $format = null;
    private ?int $count = null;
    private bool $curlCheckSSLCertificate = true;

    /**
     * You can provide an array with the different supported settings, for example
     * [
     *      'language' => 'fr',
     *      'name' => 'Stra',
     *      'format' => 'json',
     *      'count' => 42,
     *      'curlCheckSSLCertificate' => false
     * ]
     * @param array $settings
     */
    public function __construct(array $settings = []) {
        foreach ($settings as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    /**
     * Set the name parameter.
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self {
        $this->name = $name;
        return $this;
    }

    /**
     * Set the language parameter.
     * @param string $language
     * @return $this
     * @throws Exception
     */
    public function setLanguage(string $language): self {
        $language = strtolower($language);
        if (!in_array($language, self::AVAILABLE_LANGUAGES)) {
            throw new Exception("The language \"$language\" is not available. Available languages : " . implode(', ', self::AVAILABLE_LANGUAGES));
        }
        $this->language = $language;
        return $this;
    }

    /**
     * Set the format parameter
     * @param string $format
     * @return $this
     * @throws Exception
     */
    public function setFormat(string $format): self {
        $format = strtolower($format);
        if (!in_array($format, self::AVAILABLE_FORMAT)) {
            throw new Exception("The format $format is not available. Available formats : " . implode(', ', self::AVAILABLE_FORMAT));
        }
        $this->format = $format;
        return $this;
    }

    /**
     * Set the count parameter.
     * @param int $count
     * @return $this
     * @throws Exception
     */
    public function setCount(int $count): self {
        if ($count < self::MIN_COUNT || $count > self::MAX_COUNT) {
            throw new Exception("count must be between " . self::MIN_COUNT . " and " . self::MAX_COUNT . ".");
        }
        $this->count = $count;
        return $this;
    }

    /**
     * If you have trouble to retrieve data it may be caused by the SSL certificate, so with this function, you
     * can disable the check (not recommended).
     * @param bool $curlCheckSSLCertificate
     * @return $this
     */
    public function setCurlCheckSSLCertificate(bool $curlCheckSSLCertificate): self {
        $this->curlCheckSSLCertificate = $curlCheckSSLCertificate;
        return $this;
    }

    /**
     * Build the querystring part.
     * @return string
     */
    private function buildQueryString(): string {
        $queryString = "";
        if ($this->language && $this->language !== self::DEFAULT_LANGUAGE) {
            $queryString .= "&language=" . $this->language;
        }

        if ($this->format && $this->format !== self::DEFAULT_FORMAT) {
            $queryString .= "&format=" . $this->format;
        }

        if ($this->count && $this->count !== self::DEFAULT_COUNT) {
            $queryString .= "&count=" . $this->count;
        }

        return $queryString;
    }

    /**
     * Retrieve the content.
     * @param string|null $name if $name is given, it'll override $this->name.
     * @return array
     * @throws Exception
     */
    public function get(?string $name = null): array {
        if (!$name) {
            $name = $this->name;
        }

        if (strlen($name) < self::MIN_NAME_LENGTH) {
            throw new Exception("Name length must be at least " . self::MIN_NAME_LENGTH . " characters.");
        }

        if (!$this->format || $this->format === self::FORMAT_JSON) {
            $curl = curl_init(self::API_URL . "?name=$name" . $this->buildQueryString());
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, $this->curlCheckSSLCertificate);
            $res = curl_exec($curl);
            $info = curl_getinfo($curl);
            curl_close($curl);

            if ($info['http_code'] !== 200) {
                $json = json_decode($res, true);
                throw new Exception(array_key_exists("reason", $json) ? $json['reason'] : 'An error has occured.');
            }

            $json = json_decode($res, true);
            return $json['results'];
        } else {
            throw new Exception($this->format . " is currently not supported.");
        }
    }
}